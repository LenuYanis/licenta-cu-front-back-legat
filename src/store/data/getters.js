export function getUser(state) {
    return state.userCred;
}
export function getToken(state) {
    return state.token;
}

export function getProfileData(state){
    return state.profileData;
}

export function getCV(state){
    return state.CV;
}